import { Module } from '@nestjs/common';
import { mySqlProviders } from './mysql.provider';

@Module({
    providers: [...mySqlProviders],
    exports: [...mySqlProviders]
})

export class MySQLModule { }
