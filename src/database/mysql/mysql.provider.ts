import { createConnection } from 'typeorm';
import dotenv = require('dotenv');

dotenv.config();

export const conf = {
    host: process.env.SERVER_MYSQL,
    port: 3306,
    username: process.env.USER_MYSQL,
    password: process.env.PASSWORD_MYSQL,
    database: process.env.SCHEMA_MYSQL,
};

export const mySqlProviders = [
    {
        provide: 'DATABASE_CONNECTION',
        useFactory: async () =>
            await createConnection({
                type: 'mysql',
                host: conf.host,
                port: conf.port,
                username: conf.username,
                password: conf.password,
                database: conf.database,
                entities: ['./dist/modules/**/*.entity.js'],
                synchronize: true,
                logging: true,
            }),
    },
];

// "publish": "scp -r /Users/dovalgomes93/Documents/2BE/PROJETOS/nume/backend/dist/ root:Nume135@68.183.195.79:/root/ && scp -r /Users/dovalgomes93/Documents/2BE/PROJETOS/nume/backend/package.json root@68.183.195.79:/root/ && scp -r /Users/dovalgomes93/Documents/2BE/PROJETOS/nume/backend/.env && root@68.183.195.79:/root",
