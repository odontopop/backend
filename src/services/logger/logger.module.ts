import { Module } from '@nestjs/common';
import { CustomLogger } from './logger.service';

@Module({
    imports: [],
    providers: [{ provide: CustomLogger, useClass: CustomLogger }],
    exports: [
        CustomLogger
    ]
})

export class LoggerModule { }
