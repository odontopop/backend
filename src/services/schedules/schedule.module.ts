import { Module } from '@nestjs/common';
import { ScheduleService } from './schedule.service';

import { LoggerModule } from './../logger/logger.module';

@Module({
    imports: [LoggerModule],
    providers: [{ provide: ScheduleService, useClass: ScheduleService }],
    exports: [
        ScheduleService
    ]
})

export class ScheduleModule { }
