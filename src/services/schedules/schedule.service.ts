import { CustomLogger } from './../logger/logger.service';
import { Injectable } from '@nestjs/common';
import { NestSchedule, Cron, Interval, Timeout } from 'nest-schedule';

@Injectable()
export class ScheduleService extends NestSchedule {
    constructor(
        // private readonly voucherService: VoucherService,
         private readonly customLogger: CustomLogger
    ) {
        super();
    }

    // Todos os dias 01:00 - 00 01 * * *
    @Cron('00 00 01 * * *', { key: 'disable-voucher-by-expiration-date', })
    async disableVoucherByExpirationDate() {
        // const exec = await this.voucherService.inativeByExpirationDate();
        // const log = 'Schedule: disable-voucher-by-expiration-date - vouchers disabled: ' + exec.length;
        // this.customLogger.log(log, 'ScheduleService');
        return { key: 'disable-voucher-by-expiration-date', exec: true}; // data: exec };
    }
}
