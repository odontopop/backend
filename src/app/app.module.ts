// Nest
import { Module } from '@nestjs/common';
import { MulterModule } from '@nestjs/platform-express';

import { APP_FILTER } from '@nestjs/core';

// Config
import { HttpExceptionFilter } from './exeptions.filter';

import { ScheduleModule } from './../services/schedules/schedule.module';
import { LoggerModule } from './../services/logger/logger.module';
import { CompanyModule } from 'src/modules/app/company/company.module';
import { SubsidiaryModule } from 'src/modules/app/subsidiary/subsidiary.module';
import { UserModule } from 'src/modules/app/user/user.module';
import { PlanModule } from 'src/modules/app/plan/plan.module';
import { CustomerModule } from 'src/modules/app/customer/customer.module';
import { CustomerPlanModule } from 'src/modules/app/customer-plan/customer-plan.module';

@Module({
    imports: [
        // Application
        ScheduleModule,
        LoggerModule,

        // Establishment
        CompanyModule,
        SubsidiaryModule,

        // Users
        UserModule,

        // Plan
        PlanModule,

        // Customer
        CustomerModule,
        CustomerPlanModule,

        MulterModule.register({
            dest: './uploads',
        }),
    ],
    providers: [
        {
            provide: APP_FILTER,
            useClass: HttpExceptionFilter,
        },
    ],
})
export class ApplicationModule { }
