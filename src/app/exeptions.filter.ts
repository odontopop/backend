import {
    ExceptionFilter,
    Catch,
    ArgumentsHost,
    HttpException,
} from '@nestjs/common';
import { Request, Response } from 'express';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
    catch(exception: HttpException, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse<Response>();
        const request = ctx.getRequest<Request>();
        const status = exception.getStatus();

        // const message = exception.message.message[0].constraints
        //     ? exception.message.message[0].constraints
        //     : exception.getResponse();

        // const message = exception.message
        //     ? exception.message
        //     : exception.getResponse();

        let anyException: any = exception;
        let message = null;

        if (anyException.response) {
            message = anyException.response;
        } else {
            message = exception.message
                ? exception.message
                : exception.getResponse();
        }

        response.status(status).json({
            statusCode: status,
            error_message: message,
            timestamp: new Date().toISOString(),
            path: request.url,
            params: request.body,
        });
    }
}
