import { LoggerService } from '@nestjs/common';
import { ScheduleService } from './services/schedules/schedule.service';
import { NestFactory } from '@nestjs/core';
import { ApplicationModule } from './app/app.module';
import { HttpExceptionFilter } from './app/exeptions.filter';
import { CustomLogger } from './services/logger/logger.service';
import dotenv = require('dotenv');
import { static as expose } from 'express';

dotenv.config();

const port = process.env.APP_PORT || 8080;

async function bootstrap() {
    const app = await NestFactory.create(ApplicationModule, {
        logger: new CustomLogger(),
    });

    app.useGlobalFilters(new HttpExceptionFilter());
    app.select(ApplicationModule).get(ScheduleService);
    app.use(expose('uploads'));
    app.enableCors();

    await app.listen(port);

    const logger = new CustomLogger();
    logger.log('Running in Port: ' + port, 'Application Message');
}

bootstrap();
