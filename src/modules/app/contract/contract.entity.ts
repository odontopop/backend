import { Entity, PrimaryGeneratedColumn, BeforeInsert, Column } from 'typeorm';

@Entity({ name: 'contract' })
export class Contract {
    @PrimaryGeneratedColumn('increment', { type: 'bigint' })
    public id: number;

    @Column({ type: 'datetime', nullable: false })
    public createdAt: Date;

    @BeforeInsert() async prepareDataInsert() {
        this.createdAt = new Date();
    }
}
