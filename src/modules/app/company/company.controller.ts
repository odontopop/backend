import { CompanyService } from './company.service';
import {
    Controller,
    Get,
    HttpStatus,
    Headers,
    Res,
    Body,
    Post,
    Param,
    Put,
    UsePipes,
    ValidationPipe,
} from '@nestjs/common';
import { Company } from './dto/company.entity';
import { User } from '../user/decorator/user.decorator';

@Controller('api/v1/company')
export class CompanyController {
    constructor(private readonly companyService: CompanyService) {}

    @Post()
    @UsePipes(ValidationPipe)
    public async post(
        @Res() res,
        @User({ validSubsidiary: false }) header,
        @Body() company: any,
    ) {
        const save = await this.companyService.insert(company);
        return res.status(HttpStatus.OK).json({
            message: `Company Sucessfully Created`,
            status: true,
            data: save,
        });
    }

    @Put()
    @UsePipes(ValidationPipe)
    public async put(@Res() res, @User() header, @Body() company: Company) {
        const result = await this.companyService.update(company);
        return res.status(HttpStatus.OK).json({
            message: `Company Sucessfully Updated`,
            status: true,
            data: result,
        });
    }

    @Put('/enable/:id')
    @UsePipes(ValidationPipe)
    public async enable(@Res() res, @Param('id') param: string) {
        const result = await this.companyService.enable(param);
        return res.status(HttpStatus.OK).json({
            message: `Company Sucessfully Enabled`,
            status: true,
            data: result,
        });
    }

    @Put('/disable/:id')
    @UsePipes(ValidationPipe)
    public async disable(@Res() res, @Param('id') param: string) {
        const result = await this.companyService.disable(param);
        return res.status(HttpStatus.OK).json({
            message: `Company Sucessfully Disabled`,
            status: true,
            data: result,
        });
    }

    @Get('/search/:query')
    public async getBySearch(@Res() res, @Param('query') param: string) {
        const result = await this.companyService.findByLike(param);
        return res.status(HttpStatus.OK).json({
            message: `List of Companies in Query`,
            status: true,
            data: result,
        });
    }

    @Get('/active')
    public async listActive(@Res() res) {
        const result = await this.companyService.findAll(true);
        return res.status(HttpStatus.OK).json({
            message: `List of Companies Actives`,
            status: true,
            data: result,
        });
    }

    @Get('/:id')
    public async getById(@Res() res, @Param('id') param: string) {
        const result = await this.companyService.findById(param);
        return res.status(HttpStatus.OK).json({
            message: `Company by Id`,
            status: true,
            data: result,
        });
    }

    @Get()
    public async listAll(@Res() res) {
        const result = await this.companyService.findAll();
        return res.status(HttpStatus.OK).json({
            message: `List of Companies`,
            status: true,
            data: result,
        });
    }
}
