import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    ManyToOne,
    JoinColumn,
    OneToMany,
} from 'typeorm';
import { IsString, IsNotEmpty, IsBoolean } from 'class-validator';

import { Subsidiary } from '../../subsidiary/subsidiary.entity';

@Entity({ name: 'company' })
export class Company {
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column({ name: 'name', length: 200, nullable: false })
    @IsString({ message: 'Name is String' })
    @IsNotEmpty({ message: 'Name cannot be empty' })
    public name: string;

    @Column({ type: 'boolean', nullable: false, default: true })
    @IsBoolean({ message: 'Active is boolean' })
    public active: boolean;
}
