import { Connection } from 'typeorm';
import { Company } from './dto/company.entity';

export const companyRepository = [
    {
        provide: 'COMPANY_REPOSITORY',
        useFactory: (connection: Connection) => connection.getRepository(Company),
        inject: ['DATABASE_CONNECTION']
    }
]