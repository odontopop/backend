import { Company } from './dto/company.entity';
import { Injectable, Inject, BadRequestException } from '@nestjs/common';
import { Repository } from 'typeorm';

@Injectable()
export class CompanyService {
    constructor(
        @Inject('COMPANY_REPOSITORY')
        private readonly companyRepository: Repository<Company>,
    ) {}

    public async findAll(enabled?: boolean): Promise<Company[]> {
        let company = await this.companyRepository.find({
            where: [{ active: true }],
        });

        return company;
    }

    public async findByLike(paramQuery?: string): Promise<Company[]> {
        if (paramQuery.length > 0) {
            const companies = await this.companyRepository
                .createQueryBuilder('company')
                .andWhere(`company.name like :query`, {
                    query: '%' + paramQuery + '%',
                })
                .orWhere(`company.id like :query`, {
                    query: '%' + paramQuery + '%',
                })
                .getMany();
            return companies;
        } else {
            return await this.findAll();
        }
    }

    public async findByName(name: string): Promise<Company[]> {
        let company = await this.companyRepository.find({
            where: [{ name: name }],
        });

        return company;
    }

    public async findById(id: string): Promise<Company> {
        return await this.companyRepository.findOne(id, {
            relations: ['subsidiaries'],
        });
    }

    public async insert(company: Company): Promise<Company> {
        const companyLoad = await this.findByName(company.name);

        if (companyLoad) {
            throw new BadRequestException('Company already exists');
        } else {
            const insert = await this.companyRepository.save(company);
            return insert;
        }
    }

    public async update(company: Company): Promise<Company> {
        await this.companyRepository.update(company.id, company);
        return this.findById(company.id.toString());
    }

    public async enable(id: string): Promise<Company> {
        const company: Company = await this.findById(id);
        if (!company) {
            throw new BadRequestException('Company not found');
        }
        if (company.active) {
            throw new BadRequestException('Company is already active');
        }

        company.active = true;
        return await this.update(company);
    }

    public async disable(id: string): Promise<Company> {
        const company: Company = await this.findById(id);
        if (!company) {
            throw new BadRequestException('Company not found');
        }

        if (!company.active) {
            throw new BadRequestException('Company is already disabled');
        }

        company.active = false;
        return await this.update(company);
    }
}
