import { MySQLModule } from 'src/database/mysql/mysql.module';
import { Module } from '@nestjs/common';

import { CompanyService } from './company.service';
import { companyRepository } from './company.repository';
import { CompanyController } from './company.controller';


@Module({
    imports: [MySQLModule],
    controllers: [CompanyController],
    providers: [
        ...companyRepository,
        { provide: CompanyService, useClass: CompanyService }
    ],
    exports: [CompanyService]
})
export class CompanyModule { }
