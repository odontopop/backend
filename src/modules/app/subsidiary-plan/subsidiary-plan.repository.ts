import { Connection } from 'typeorm';
import { SubsidiaryPlan } from './subsidiary-plan.entity';

export const SubsidiaryPlanRepository = [
    {
        provide: 'SUBSIDIARY_PLAN_REPOSITORY',
        useFactory: (connection: Connection) =>
            connection.getRepository(SubsidiaryPlan),
        inject: ['DATABASE_CONNECTION'],
    },
];
