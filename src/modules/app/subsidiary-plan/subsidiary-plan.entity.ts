import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    ManyToOne,
    JoinColumn,
    OneToOne,
    OneToMany,
} from 'typeorm';
import {
    IsString,
    IsNotEmpty,
    IsBoolean,
    IsDate,
    IsNumber,
    IsEnum,
} from 'class-validator';
import { Subsidiary } from '../subsidiary/subsidiary.entity';
import { Plan } from '../plan/plan.entity';

@Entity({ name: 'subsidiary_plan' })
export class SubsidiaryPlan {
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @IsNotEmpty({ message: 'Subsidiary is Required' })
    @ManyToOne((type) => Subsidiary, (subsidiary) => subsidiary.subsidiaryPlan, {
        nullable: false,
        onDelete: 'CASCADE',
    })
    @JoinColumn({ name: 'id_subsidiary' })
    public subsidiary: Subsidiary | string;

    @IsNotEmpty({ message: 'Plan is Required' })
    @ManyToOne((type) => Plan, (plan) => plan.id, {
        nullable: false,
        onDelete: 'CASCADE',
    })
    @JoinColumn({ name: 'id_plan' })
    public plan: Plan | string;

    @Column({ type: 'boolean', nullable: false, default: true })
    @IsBoolean({ message: 'Active is boolean' })
    @IsNotEmpty({ message: 'Active cannot be empty' })
    public active: boolean;
}
