import { Injectable, Inject } from '@nestjs/common';
import { Repository, getManager } from 'typeorm';
import { SubsidiaryPlan } from './subsidiary-plan.entity';

@Injectable()
export class SubsidiaryPlanService {
    constructor(
        @Inject('SUBSIDIARY_PLAN_REPOSITORY')
        private readonly SubsidiaryPlanRepository: Repository<SubsidiaryPlan>,
    ) {}
}
