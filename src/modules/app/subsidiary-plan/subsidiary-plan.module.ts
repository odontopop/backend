import { MySQLModule } from 'src/database/mysql/mysql.module';
import { Module } from '@nestjs/common';

import { SubsidiaryPlanController } from './subsidiary-plan.controller';
import { SubsidiaryPlanRepository } from './subsidiary-plan.repository';
import { SubsidiaryPlanService } from './subsidiary-plan.service';

@Module({
    imports: [MySQLModule],
    controllers: [SubsidiaryPlanController],
    providers: [
        ...SubsidiaryPlanRepository,
        {
            provide: SubsidiaryPlanService,
            useClass: SubsidiaryPlanService,
        },
    ],
    exports: [SubsidiaryPlanService],
})
export class SubsidiaryPlanModule {}
