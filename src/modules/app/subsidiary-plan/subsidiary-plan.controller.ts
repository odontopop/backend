import {
    Controller,
    Get,
    HttpStatus,
    Headers,
    Res,
    Body,
    Post,
    Param,
    Put,
    UsePipes,
    ValidationPipe,
    Query,
} from '@nestjs/common';
import { SubsidiaryPlanService } from './subsidiary-plan.service';

@Controller('api/v1/evaluation-moviment')
export class SubsidiaryPlanController {
    constructor(
        private readonly subsidiaryPlanService: SubsidiaryPlanService,
    ) {}
}
