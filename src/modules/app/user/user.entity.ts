import {
    IsString,
    IsNotEmpty,
    IsPhoneNumber,
    IsEmail,
    IsOptional,
} from 'class-validator';
import {
    PrimaryGeneratedColumn,
    Column,
    Entity,
    OneToMany,
    BeforeInsert,
    BeforeUpdate,
    ManyToOne,
} from 'typeorm';

import { IsBoolean } from 'class-validator';
import { OneToOne, JoinColumn } from 'typeorm';

import { Subsidiary } from '../subsidiary/subsidiary.entity';
import { ProfileAccount } from './enumerables/profile.account.enum';
import { StatusAccount } from './enumerables/status.account.enum';
import { TypeGender } from './enumerables/enum.gender';

@Entity('user')
export class User {
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @IsString({ message: 'Name is String' })
    @IsNotEmpty({ message: 'Name cannot be empty' })
    @Column({ name: 'name', length: 200 })
    public name: string;

    @IsString({ message: 'Nickname is String' })
    @IsNotEmpty({ message: 'Nickname cannot be empty' })
    @Column({ name: 'nickname', length: 200 })
    public nickname: string;

    // @IsPhoneNumber('+55', { message: 'Phone is not valid' })
    @IsNotEmpty({ message: 'Phone cannot be empty' })
    @Column({ name: 'phone', length: 200, nullable: true })
    public phone: string;

    @IsString({ message: 'Password is String' })
    @IsNotEmpty({ message: 'Password cannot be empty' })
    @Column({ name: 'password', length: 200, select: false })
    public password: string;

    @IsEmail({}, { message: 'Mail is invalid' })
    @IsNotEmpty({ message: 'Mail cannot be empty' })
    @Column({ name: 'mail', length: 200, nullable: true })
    public mail: string;

    //@IsNotEmpty({ message: 'Subsidiary is Required' })
    @ManyToOne((type) => Subsidiary, (subsidiary) => subsidiary.user)
    @JoinColumn({ name: 'id_subsidiary' })
    public subsidiary: Subsidiary;

    @Column({ name: 'cpf', nullable: true })
    public cpf: string;

    @Column({ name: 'profile_account' })
    public profile: ProfileAccount;

    @Column({ name: 'gender', nullable: true })
    @IsNotEmpty({ message: 'Gender cannot be empty' })
    public gender: TypeGender;

    @IsOptional()
    @Column({ name: 'status_account', default: StatusAccount.ENABLED })
    public status: StatusAccount;

    @BeforeInsert() async prepareDataInsert() {
        if (this.cpf) {
            this.cpf = this.cpf.replace(/[^0-9]/g, '');
        }

        if (this.phone) {
            this.phone = this.phone.replace(/[^0-9]/g, '');
        }
    }
}
