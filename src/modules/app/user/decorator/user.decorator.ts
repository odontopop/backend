import {
    createParamDecorator,
    BadRequestException,
    ExecutionContext,
} from '@nestjs/common';
import * as jwt from 'jsonwebtoken';

export interface UserDecoratorInterface {
    userId: string;
    subsidiaryId: string;
}

export interface UserValidDecoratorInterface {
    validToken?: boolean;
    validSubsidiary?: boolean;
}

export const User = createParamDecorator(
    async (data, req: ExecutionContext) => {
        let token: string = '';
        let subsidiary: string = null;
        let validSubsidiary = true;
        let validToken = true;
        let request = req.switchToHttp().getRequest();

        let user: UserDecoratorInterface = {
            userId: null,
            subsidiaryId: null,
        };

        if (data) {
            if (data.hasOwnProperty('validSubsidiary')) {
                if (data.validSubsidiary) {
                    validSubsidiary = true;
                } else {
                    validSubsidiary = false;
                }
            } else {
                validSubsidiary = true;
            }

            if (data.hasOwnProperty('validToken')) {
                if (data.validToken) {
                    validToken = true;
                } else {
                    validToken = false;
                }
            } else {
                validToken = true;
            }
        }

        if (validSubsidiary) {
            if (request.headers.subsidiaryid) {
                subsidiary = request.headers.subsidiaryid;
            } else {
                throw new BadRequestException(
                    'Not informed the subsidiaryId in the header',
                );
            }
        }

        if (validToken) {
            if (request.headers.authorization) {
                token = request.headers.authorization.replace('Bearer ', '');
            } else {
                throw new BadRequestException(
                    'Token authorization not informed',
                );
            }

            if (!token || token.length === 0) {
                throw new BadRequestException('Invalid token');
            }

            const decoded: any = await jwt.decode(token);

            if (decoded) {
                user.userId = decoded.ID;
                user.subsidiaryId = subsidiary;

                return user;
            } else {
                throw new BadRequestException('Invalid token');
            }
        } else {
            user.userId = null;
            user.subsidiaryId = subsidiary;

            return user;
        }
    },
);
