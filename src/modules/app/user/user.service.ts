import {
    Injectable,
    Inject,
    BadRequestException,
    UnauthorizedException,
    OnModuleInit,
    forwardRef,
} from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Repository } from 'typeorm';

// Entities
import { User } from './user.entity';

// Interfaces
import { Credential } from './interfaces/credential';
import { Strategy } from 'passport-strategy';
import { JwtService } from '@nestjs/jwt';
import passport = require('passport');
import { SignUser } from './interfaces/sign.user.interface';
import 'dotenv/config';
import * as jwt from 'jsonwebtoken';

import { ProfileAccount } from './enumerables/profile.account.enum';
import { UserDecoratorInterface } from './decorator/user.decorator';
import { CompanyService } from '../company/company.service';
import { SubsidiaryService } from '../subsidiary/subsidiary.service';
import { StatusAccount } from './enumerables/status.account.enum';
import { TypeSourceLogin } from './interfaces/sign.user.interface.type.source';

@Injectable()
export class UserService
    extends PassportStrategy(Strategy, passport.authenticate('local'))
    implements OnModuleInit {
    constructor(
        @Inject('USER_REPOSITORY')
        private readonly userRepository: Repository<User>,
        private readonly jwtService: JwtService,
        private readonly companyService: CompanyService,
        private readonly subsidiaryService: SubsidiaryService,
    ) {
        super();
    }

    async onModuleInit() {
        //await this.initRegisters();
    }

    private async initRegisters() {
        const nume = new User();
        nume.nickname = 'admin';
        nume.mail = 'wlysses@gmail.com';
        nume.password = 'bass';
        nume.name = 'Administrator';
        nume.phone = '+55279999-99999';
        nume.profile = ProfileAccount.ADMINISTRATOR;
        nume.status = StatusAccount.ENABLED;

        //await this.addUser(nume);
    }

    private static getUserObject(
        user: User,
        includeToken = true,
    ): SignUser {
        const userObject: SignUser = {
            id: user.id,
            name: user.name,
            cpf: user.cpf,
            nickname: user.nickname,
            mail: user.mail,
            phone: user.phone,
            profile: user.profile,
            gender: user.gender,
            subsidiary: user.subsidiary
        };

        if (includeToken) {
            userObject.token = jwt.sign(
                {
                    ID: user.id,
                    NICKNAME: user.nickname,
                    MAIL: user.mail,
                },
                process.env.JWT_SECRET,
                { expiresIn: '1y' },
            );
        }

        return userObject;
    }

    public async authenticate(credential: Credential): Promise<SignUser> {

        const userQuery = await this.userRepository
            .createQueryBuilder('user')
            .leftJoinAndSelect('user.subsidiary', 'subsidiary')
            .where(`(user.nickname = :nickname or user.mail = :mail)`, {
                nickname: credential.source,
                mail: credential.source,
            })
            .andWhere(`user.password = :password`, {
                password: credential.password,
            });

        const USER: User = await userQuery.getOne();

        if (USER) {

        } else {
            throw new UnauthorizedException('Access not Authorized');
        }

        // if (USER) {
        //     if (USER.status !== StatusAccount.ENABLED) {
        //         throw new UnauthorizedException(
        //             USER.status,
        //             'Access not Authorized',
        //         );
        //     }
        // }

        return UserService.getUserObject(USER);
    }

    public async login(credential: Credential) {
        return await this.authenticate(credential);
    }

    // Select All
    public async selectAll(): Promise<User[]> {
        return await this.userRepository
            .createQueryBuilder('user')
            .leftJoinAndSelect('user.subsidiary', 'subsidiary')
            .getMany()

    }

    public async findByCpf(cpf: string): Promise<User> {
        const user = await this.userRepository
            .createQueryBuilder('user')
            .leftJoinAndSelect('user.userBartender', 'bartender')
            .andWhere('user.cpf = :cpf', {
                cpf,
            })
            .getOne();

        return user;
    }

    public async selectById(id: string): Promise<User> {
        return await this.userRepository.findOne(id);
    }

    public async changeProfile(header, user: User): Promise<User> {

        user.phone = user.phone.replace('(', '');

        user.phone = user.phone.replace(')', '');

        user.phone = user.phone.replace('-', '');

        user.phone = user.phone.replace(' ', '');

        let userChange: User = await this.selectById(header.userId);

        Object.assign(userChange, user);

        userChange = await this.userRepository.create(userChange);

        userChange = await this.userRepository.save(userChange);

        return userChange;
    }

    public async addUser(user: User): Promise<SignUser> {
        const inserted = await this.insert(user);
        return inserted;
    }

    private async insert(user: User, profile: ProfileAccount = null) {
        const check = await this.checkExists(user, true);
        if (check) {
            throw new BadRequestException(
                'There is already a user with this nickname or email',
            );
        }

        user.phone = user.phone.replace('(', '');

        user.phone = user.phone.replace(')', '');

        user.phone = user.phone.replace('-', '');

        user.phone = user.phone.replace(' ', '');

        const insert = await this.userRepository.save(user);

        return this.authenticate({
            source: insert.nickname,
            password: insert.password,
        });
    }

    // Update
    public async update(user: User): Promise<User> {

        user.phone = user.phone.replace('(', '');

        user.phone = user.phone.replace(')', '');

        user.phone = user.phone.replace('-', '');

        user.phone = user.phone.replace(' ', '');

        await this.userRepository.update(user.id, user);
        return this.selectById(user.id.toString());
    }

    private async checkExists(verify: User, getAll?: boolean) {
        const user = this.userRepository
            .createQueryBuilder('users')
            .orWhere(`(users.nickname = :nickname or users.mail = :mail)`, {
                nickname: verify.nickname,
                mail: verify.mail,
            });

        if (!getAll) {
            user.andWhere(`users.status = :query`, { query: true });
        }

        const result = await user.getMany();
        return result.length > 0;
    }

    public async checkEmail(verifyMail: string): Promise<boolean> {
        const user = await this.userRepository
            .createQueryBuilder('users')
            .andWhere(`users.mail = :query`, { query: verifyMail })
            .getMany();
        return user.length > 0;
    }

    public async checkNickname(verifyNickname: string) {
        const user = await this.userRepository
            .createQueryBuilder('users')
            .andWhere(`users.nickname = :query`, { query: verifyNickname })
            .getMany();
        return user.length > 0;
    }

    public async selectByNickName(header, nickname: string): Promise<User> {
        const user = await this.userRepository.findOne({
            relations: ['vouchers'],
            where: { nickname: nickname.toLowerCase() },
        });

        if (!user) {
            throw new BadRequestException(
                'There is no user with this nickname',
            );
        }

        return user;
    }
}
