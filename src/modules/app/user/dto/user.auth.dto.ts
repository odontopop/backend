import { IsString, IsNotEmpty } from 'class-validator';

export class UserAuthDTO {

    @IsString({ message: 'Header -> Name is String' })
    @IsNotEmpty({ message: 'Header -> id cannot be empty' })
    public id: string;

    public subsidiaryId: string;

    @IsNotEmpty({ message: 'Header -> tested cannot be empty' })
    public tested: string;

}
