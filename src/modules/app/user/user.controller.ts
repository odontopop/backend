// import { Address } from './dto/address';

// Nest
import {
    Controller,
    Post,
    Put,
    Get,
    Delete,
    Res,
    Body,
    HttpStatus,
    Param,
    UsePipes,
    ValidationPipe,
    UseGuards,
    Req,
} from '@nestjs/common';
import { BadRequestException } from '@nestjs/common';

// Services
import { UserService } from './user.service';

// DTO
import { Credential } from './interfaces/credential';
import { AuthGuard } from '@nestjs/passport';
import { User } from './user.entity';
import { User as UserHeader } from './decorator/user.decorator';

// import { CreateUser } from './dto/create.user';
// import { UpdateUser } from './dto/update.user';
// import { Login } from './interfaces/login';
// import { WithdrawVoucher } from './interfaces/withdraw.voucher';

@Controller('api/v1/users')
export class UserController {
    constructor(private readonly userService: UserService) { }

    @Post('/login')
    @UsePipes()
    public async login(@Body() credential: Credential, @Res() res) {
        const auth = await this.userService.login(credential);
        return res.status(HttpStatus.OK).json({
            message: 'Successfully authenticated user',
            status: true,
            data: auth,
        });
    }

    @Put('/user')
    @UsePipes(ValidationPipe)
    async changeProfile(
        @Res() response,
        @UserHeader({ validSubsidiary: false }) header,
        @Body() user,
    ) {
        const users = await this.userService.changeProfile(header, user);
        return response.status(HttpStatus.CREATED).json({
            message: 'User Sucessfully Updated',
            status: true,
            data: users,
        });
    }

    @Post('/user')
    @UsePipes(ValidationPipe)
    async addUser(@Res() response, @Body() user: User) {
        const users = await this.userService.addUser(user);
        return response.status(HttpStatus.CREATED).json({
            message: 'Consumer Sucessfully Created',
            status: true,
            data: users,
        });
    }

    // @Put('/add-address/')
    // @UsePipes(ValidationPipe) // @Body('nickname') nickname: string, @Body() address: Address
    // async addAddress(@Res() response, @Body('nickname') nickname: string, @Body('address') address: Address) {
    //     const user = await this.userService.addAddress(nickname, address);
    //     return response.status(HttpStatus.CREATED).json({
    //         message: 'Successfully added address',
    //         status: true,
    //         data: user
    //     });
    // }

    // @Post('/withdraw-voucher')
    // @UsePipes(ValidationPipe)
    // public async withdrawVoucher(@Res() response, @Body() body: WithdrawVoucher) {
    //   const withdraw = await this.userService.withdrawVoucher(body);
    //   return response.status(HttpStatus.OK).json({
    //     message: 'User Successfully Withdrawed Voucher',
    //     status: true,
    //     data: withdraw
    //   });
    // }

    // @Put()
    // public async update(@Res() response, @Body() updateUserDto: User) {
    //     const update = await this.userService.update(updateUserDto);
    //     return response.status(HttpStatus.OK).json({
    //         message: 'User has Been Altered',
    //         status: true,
    //         data: update
    //     });
    // }

    // @Delete(':id')
    // async delete(@Res() response, @Param('id') id: string) {
    //     const deleted = this.userService.delete(id);
    //     return response.status(HttpStatus.OK).json({
    //         message: 'User has Been Deleted',
    //         status: true,
    //         data: deleted
    //     });
    // }

    @Get()
    async listAll(@Res() response) {
        const users = await this.userService.selectAll();
        return response.status(HttpStatus.OK).json({
            message: 'Users has Been Selected',
            status: true,
            data: users,
        });
    }

    @Get('/user')
    public async listUser(
        @Res() res,
        @UserHeader({ validSubsidiary: false }) header,
    ) {
        const result = await this.userService.selectById(header.userId);
        return res.status(HttpStatus.OK).json({
            message: `List User`,
            status: true,
            data: result,
        });
    }

    // @Get('/:id')
    // async get(@Param('id') id: string) {
    //     return await this.userService.selectById(id);
    // }

    @Get('/check-mail/:mail')
    async checkMailUser(@Res() response, @Param('mail') mail: string) {
        const status = await this.userService.checkEmail(mail);
        return response.status(HttpStatus.OK).json({
            message: 'Status User By Email',
            status: true,
            data: status,
        });
    }

    @Get('/check-nickname/:nickname')
    async checkNicknameUser(
        @Res() response,
        @Param('nickname') nickname: string,
    ) {
        const status = await this.userService.checkNickname(nickname);
        return response.status(HttpStatus.OK).json({
            message: 'Status User By nickname',
            status: true,
            data: status,
        });
    }

    @Get('consume')
    // @UseGuards(new AuthGuard())
    public async consumeResource() {
        return 'Authorized';
    }
}
