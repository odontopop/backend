import { MySQLModule } from 'src/database/mysql/mysql.module';
import { Module, forwardRef } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';

import { userRepository } from './user.repository';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { CompanyModule } from '../company/company.module';
import { SubsidiaryModule } from '../subsidiary/subsidiary.module';

// export const JWT_NUME_TOKEN = {
//     secret: 'nUm34p1',
// };
@Module({
    imports: [
        MySQLModule,
        forwardRef(() => CompanyModule),
        forwardRef(() => SubsidiaryModule),
        JwtModule.register({
            secret: process.env.JWT_SECRET,
            signOptions: { expiresIn: process.env.JWT_EXPIRES_IN },
        }),
    ],
    controllers: [UserController],
    providers: [
        ...userRepository,
        { provide: UserService, useClass: UserService },
    ],
    exports: [UserService, JwtModule],
})
export class UserModule {}
