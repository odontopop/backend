export enum TypeGender {
    Male = 'Male',
    Female = 'Female',
    Both = 'Both',
}
