export enum StatusAccount {
  'DISABLED' = 'DISABLED',
  'ENABLED' = 'ENABLED',
  'BANED' = 'BANED',
  'BLOCKED' = 'BLOCKED'
}
