import { IsNotEmpty, IsString } from 'class-validator';

export class Credential {
    @IsString({ message: 'Source is string' })
    @IsNotEmpty({ message: 'Souce cannot be empty' })
    public source: string;

    @IsString({ message: 'Password is String' })
    @IsNotEmpty({ message: 'Password cannot be empty' })
    public readonly password?: string;
}
