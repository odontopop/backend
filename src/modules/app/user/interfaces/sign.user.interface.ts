import { ProfileAccount } from '../enumerables/profile.account.enum';
import { TypeGender } from '../enumerables/enum.gender';

export interface SignUser {
    id: string;
    name: string;
    cpf: string;
    nickname: string;
    mail: string;
    token?: string;
    phone: string;

    subsidiary: any;
    profile: ProfileAccount;
    gender: TypeGender;
}
