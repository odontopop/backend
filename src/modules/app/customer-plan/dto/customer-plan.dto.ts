import {
    IsString,
    IsNotEmpty,
    IsNumber,
    IsEnum,
    IsBoolean,
    IsDate,
    IsDateString,
} from 'class-validator';

export class CustomerPlanDto {

    // @IsString({ message: 'Id is String' })
    // @IsNotEmpty({ message: 'Id cannot be empty' })
    public id: string;

    @IsString({ message: 'Name is String' })
    @IsNotEmpty({ message: 'Name cannot be empty' })
    public name: string;

    @IsString({ message: 'Cpf is String' })
    @IsNotEmpty({ message: 'Cpf cannot be empty' })
    public cpf: string;

    @IsString({ message: 'planId is String' })
    @IsNotEmpty({ message: 'planId cannot be empty' })
    public planId: string;

    //@IsDateString({ message: 'dateSignature Date is ISO Date String' })
    // @IsDate()
    public dateSignature: Date;

    // @IsDate({ message: 'dateReview is date' })
    // public dateReview: Date;

    // @IsDate({ message: 'dateRenewal is date' })
    // public dateRenewal: Date;

    @IsBoolean({ message: 'loyaltyCard is boolean' })
    public loyaltyCard: boolean;

    @IsBoolean({ message: 'Active is boolean' })
    public active: boolean;
}
