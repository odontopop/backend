import { Connection } from 'typeorm';
import { CustomerPlan } from './customer-plan.entity';

export const CustomerPlanRepository = [
    {
        provide: 'SUBSIDIARY_PLAN_REPOSITORY',
        useFactory: (connection: Connection) =>
            connection.getRepository(CustomerPlan),
        inject: ['DATABASE_CONNECTION'],
    },
];
