import { MySQLModule } from 'src/database/mysql/mysql.module';
import { Module } from '@nestjs/common';

import { CustomerPlanController } from './customer-plan.controller';
import { CustomerPlanRepository } from './customer-plan.repository';
import { CustomerPlanService } from './customer-plan.service';

@Module({
    imports: [MySQLModule],
    controllers: [CustomerPlanController],
    providers: [
        ...CustomerPlanRepository,
        {
            provide: CustomerPlanService,
            useClass: CustomerPlanService,
        },
    ],
    exports: [CustomerPlanService],
})
export class CustomerPlanModule { }
