import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    ManyToOne,
    JoinColumn,
    OneToOne,
    OneToMany,
} from 'typeorm';
import {
    IsString,
    IsNotEmpty,
    IsBoolean,
    IsDate,
    IsNumber,
    IsEnum,
} from 'class-validator';
import { Subsidiary } from '../subsidiary/subsidiary.entity';
import { Plan } from '../plan/plan.entity';
import { Customer } from '../customer/customer.entity';

@Entity({ name: 'customer_plan' })
export class CustomerPlan {
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @IsNotEmpty({ message: 'Subsidiary is Required' })
    @ManyToOne((type) => Subsidiary, (subsidiary) => subsidiary.customerPlan, {
        nullable: false,
        onDelete: 'CASCADE',
    })
    @JoinColumn({ name: 'id_subsidiary' })
    public subsidiary: Subsidiary | string;

    @IsNotEmpty({ message: 'Customer is Required' })
    @OneToOne((type) => Customer, (customer) => customer.customerPlan, {
        nullable: false,
        onDelete: 'CASCADE',
    })
    @JoinColumn({ name: 'id_customer' })
    public customer: Customer | string;

    @IsNotEmpty({ message: 'Plan is Required' })
    @ManyToOne((type) => Plan, (plan) => plan.id, {
        nullable: false,
        onDelete: 'CASCADE',
    })
    @JoinColumn({ name: 'id_plan' })
    public plan: Plan | string;

    // @IsString({ message: 'Contract is String' })
    @IsNotEmpty({ message: 'Name cannot be empty' })
    @Column({ name: 'contract', length: 200 })
    public contract: string;

    @Column({ name: 'date_signature', type: 'date', nullable: true })
    @IsDate({ message: 'Active is date' })
    public dateSignature: Date;

    @Column({ name: 'date_review', type: 'date', nullable: true })
    @IsDate({ message: 'Active is date' })
    public dateReview: Date;

    @Column({ name: 'date_renewal', type: 'date', nullable: true })
    @IsDate({ message: 'Active is date' })
    public dateRenewal: Date;

    @Column({ name: "loyalty_card", type: 'boolean', nullable: false, default: true })
    @IsBoolean({ message: 'LoyaltyCard is boolean' })
    @IsNotEmpty({ message: 'LoyaltyCard cannot be empty' })
    public loyaltyCard: boolean
}
