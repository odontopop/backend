import {
    Controller,
    Get,
    HttpStatus,
    Headers,
    Res,
    Body,
    Post,
    Param,
    Put,
    UsePipes,
    ValidationPipe,
    Query,
} from '@nestjs/common';
import { CustomerPlanService } from './customer-plan.service';

@Controller('api/v1/evaluation-moviment')
export class CustomerPlanController {
    constructor(
        private readonly customerPlanService: CustomerPlanService,
    ) { }
}
