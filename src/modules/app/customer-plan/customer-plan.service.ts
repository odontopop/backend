import { Injectable, Inject } from '@nestjs/common';
import { Repository, getManager } from 'typeorm';
import { CustomerPlan } from './customer-plan.entity';

@Injectable()
export class CustomerPlanService {
    constructor(
        @Inject('SUBSIDIARY_PLAN_REPOSITORY')
        private readonly CustomerPlanRepository: Repository<CustomerPlan>,
    ) { }
}
