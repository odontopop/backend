import { MySQLModule } from 'src/database/mysql/mysql.module';
import { Module } from '@nestjs/common';

import { CustomerService } from './customer.service';
import { customerRepository } from './customer.repository';
import { CustomerController } from './customer.controller';

@Module({
    imports: [MySQLModule],
    controllers: [CustomerController],
    providers: [
        ...customerRepository,
        { provide: CustomerService, useClass: CustomerService },
    ],
    exports: [CustomerService],
})
export class CustomerModule { }
