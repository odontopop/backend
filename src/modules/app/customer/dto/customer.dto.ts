import {
    IsString,
    IsNotEmpty,
    IsNumber,
    IsEnum,
    IsBoolean,
} from 'class-validator';

export class CustomerDto {
    @IsString({ message: 'Id is String' })
    @IsNotEmpty({ message: 'Id cannot be empty' })
    public id: string;
}
