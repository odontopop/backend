import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    ManyToOne,
    JoinColumn,
    JoinTable,
    OneToMany,
    AfterLoad,
    OneToOne,
} from 'typeorm';

import { IsString, IsNotEmpty, IsBoolean, IsNumber } from 'class-validator';
import { CustomerPlan } from '../customer-plan/customer-plan.entity';

@Entity({ name: 'customer' })
export class Customer {
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column({ name: 'name', length: 200, nullable: false, default: '' })
    @IsString({ message: 'Name is String' })
    @IsNotEmpty({ message: 'Name cannot be empty' })
    public name: string;

    @Column({ name: 'cpf', length: 11, nullable: false, default: '' })
    @IsString({ message: 'Cpf is String' })
    @IsNotEmpty({ message: 'Cpf cannot be empty' })
    public cpf: string;

    @OneToOne((type) => CustomerPlan, (customerPlan) => customerPlan.customer, {
        nullable: true,
        cascade: true,
    })
    public customerPlan: CustomerPlan;

    @Column({ type: 'boolean', nullable: false, default: true })
    @IsBoolean({ message: 'Active is boolean' })
    @IsNotEmpty({ message: 'Active cannot be empty' })
    public active: boolean;
}