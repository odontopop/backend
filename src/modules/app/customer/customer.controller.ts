import {
    Controller,
    Get,
    HttpStatus,
    Headers,
    Res,
    Body,
    Post,
    Param,
    Put,
    Delete,
    ValidationPipe,
    UsePipes,
    Query
} from '@nestjs/common';
import { CustomerService } from './customer.service';
import { User } from '../user/decorator/user.decorator';
import { CustomerPlanDto } from '../customer-plan/dto/customer-plan.dto';

@Controller('api/v1/customer')
export class CustomerController {
    constructor(private readonly customerService: CustomerService) { }

    @Post()
    @UsePipes(ValidationPipe)
    public async post(
        @Res() res,
        @User({ validSubsidiary: true }) header,
        @Body() customerPlan: CustomerPlanDto,
    ) {
        const save = await this.customerService.save(header, customerPlan);
        return res.status(HttpStatus.OK).json({
            message: `CustomerPlan Sucessfully Created`,
            status: true,
            data: save,
        });
    }

    @Put()
    @UsePipes(ValidationPipe)
    public async put(
        @Res() res,
        @User({ validSubsidiary: true }) header,
        @Body() customerPlan: CustomerPlanDto,
    ) {
        const save = await this.customerService.save(header, customerPlan, false);
        return res.status(HttpStatus.OK).json({
            message: `CustomerPlan Sucessfully Updated`,
            status: true,
            data: save,
        });
    }

    @Get()
    public async listAll2(@Res() res, @User({ validSubsidiary: true }) header, @Query('page') page: number,) {
        const result = await this.customerService.findCustomers(header, null, { page });
        return res.status(HttpStatus.OK).json({
            message: `PDF`,
            status: true,
            data: result,
        });
    }

    @Get('/report/list')
    public async listAll(@Res() res, @User({ validSubsidiary: true }) header, @Query('dateStart') dateStart: Date,
        @Query('dateEnd') dateEnd: Date) {
        const result = await this.customerService.getReportCustomers(header, { dateStart, dateEnd });
        return res.status(HttpStatus.OK).json({
            message: `PDF`,
            status: true,
            data: result,
        });
    }

    @Get('/report/listDetail')
    public async listAllMonthly(@Res() res, @User({ validSubsidiary: true }) header, @Query('dateStart') dateStart: Date,
        @Query('dateEnd') dateEnd: Date) {
        const result = await this.customerService.getReportCustomersMonthly(header, { dateStart, dateEnd });
        return res.status(HttpStatus.OK).json({
            message: `PDF`,
            status: true,
            data: result,
        });
    }
}
