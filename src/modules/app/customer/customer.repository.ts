import { Connection } from 'typeorm';
import { Customer } from './customer.entity';

export const customerRepository = [
    {
        provide: 'CUSTOMER',
        useFactory: (connection: Connection) => connection.getRepository(Customer),
        inject: ['DATABASE_CONNECTION'],
    },
];
