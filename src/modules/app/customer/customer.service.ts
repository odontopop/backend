import { Customer } from './customer.entity';
import {
    Injectable,
    Inject,
    BadRequestException,
    Head,
    Header,
} from '@nestjs/common';

import { Repository, getManager, SelectQueryBuilder } from 'typeorm';
import { UserDecoratorInterface } from '../user/decorator/user.decorator';
import { CustomerPlanDto } from '../customer-plan/dto/customer-plan.dto';
import { CustomerPlan } from '../customer-plan/customer-plan.entity';
import { Contract } from '../contract/contract.entity';
import { Plan } from '../plan/plan.entity';

@Injectable()
export class CustomerService {
    constructor(
        @Inject('CUSTOMER')
        private readonly customerRepository: Repository<Customer>,
    ) { }

    public async save(header: UserDecoratorInterface, customerPlanDto: CustomerPlanDto, toInsert: boolean = true): Promise<any> {
        let customerLoad: Partial<Customer> = {};
        let customerPlanLoad: Partial<CustomerPlan> = {};

        await getManager().transaction(async (transactionalEntityManager) => {

            let customer = await this.findByCpf(customerPlanDto.cpf, transactionalEntityManager);

            if (toInsert) {
                if (customer) {
                    throw new BadRequestException('Customer already exists');
                }
            } else {
                if (!customer) {
                    throw new BadRequestException('Customer not found');
                }
            }

            customerLoad = {
                id: toInsert ? undefined : customer.id,
                name: customerPlanDto.name,
                cpf: customerPlanDto.cpf,
                active: customerPlanDto.active
            }

            customerLoad = await transactionalEntityManager.create(
                Customer,
                customerLoad,
            );

            customerLoad = await transactionalEntityManager.save(
                customerLoad,
            );

            let contract = null;

            if (toInsert) {
                contract = await transactionalEntityManager.create(
                    Contract,
                    {}
                );

                contract = await transactionalEntityManager.save(
                    contract,
                );

            } else {
                contract = { id: customer.customerPlan.contract };
            }

            let plan = await this.findPlanId(customerPlanDto.planId, transactionalEntityManager);

            if (!plan) {
                throw new BadRequestException('Plan not found');
            }

            let dateRenewal = new Date(customerPlanDto.dateSignature);
            let dateReview = new Date(customerPlanDto.dateSignature);

            dateRenewal.setMonth(dateRenewal.getMonth() + plan.parcel);
            dateReview.setMonth(dateRenewal.getMonth() + (plan.parcel / 2));

            customerPlanLoad = {
                id: toInsert ? undefined : customer.customerPlan.id,
                plan: customerPlanDto.planId,
                contract: ('0000000000' + contract.id.toString()).slice(-10),
                customer: customerLoad.id,
                loyaltyCard: customerPlanDto.loyaltyCard,
                dateSignature: customerPlanDto.dateSignature,
                dateRenewal: dateRenewal,
                dateReview: dateReview,
                subsidiary: header.subsidiaryId
            }

            customerPlanLoad = await transactionalEntityManager.create(
                CustomerPlan,
                customerPlanLoad,
            );

            customerPlanLoad = await transactionalEntityManager.save(
                customerPlanLoad,
            );

            // customerLoad.plan = customerPlanLoad;

        });

        return await this.findByCpf(customerPlanDto.cpf);
    }

    public async findPlanId(IdPlan: string, transactionalEntityManager: any = null): Promise<any> {

        let queryBuilderMoviment: SelectQueryBuilder<Plan> = null;

        queryBuilderMoviment = transactionalEntityManager.createQueryBuilder(
            Plan,
            'plan',
        );

        queryBuilderMoviment
            .where('plan.id = :IdPlan', {
                IdPlan,
            })

        const plan = await queryBuilderMoviment.getOne();

        return plan;
    }

    public async findByCpf(cpf: string, transactionalEntityManager: any = null): Promise<any> {

        let queryBuilderMoviment: SelectQueryBuilder<Customer> = null;

        if (transactionalEntityManager) {
            queryBuilderMoviment = transactionalEntityManager.createQueryBuilder(
                Customer,
                'customer',
            );
        } else {
            queryBuilderMoviment = this.customerRepository.createQueryBuilder(
                'customer',
            );
        }

        queryBuilderMoviment
            .leftJoinAndSelect(
                'customer.customerPlan',
                'customerPlan'
            )
            // .where('customer.subsidiary = :idsub', {
            //     idsub: header.subsidiaryId,
            // })
            .where('customer.cpf = :cpf', {
                cpf,
            })

        const customer = await queryBuilderMoviment.getOne();

        return customer;
    }

    public async findCustomers(header: UserDecoratorInterface, transactionalEntityManager: any = null, filter: any): Promise<any> {

        let queryBuilderMoviment: SelectQueryBuilder<Customer> = null;
        let filterPeriod = '';

        if (transactionalEntityManager) {
            queryBuilderMoviment = transactionalEntityManager.createQueryBuilder(
                CustomerPlan,
                'customer',
            );
        } else {
            queryBuilderMoviment = this.customerRepository.createQueryBuilder(
                'customer',
            );
        }

        if (filter.dateStart && filter.dateEnd) {
            filterPeriod = `customerPlan.dateSignature BETWEEN '${filter.dateStart}' AND '${filter.dateEnd}'`;
        } else {
            filterPeriod = `1 = 1`;
        }

        queryBuilderMoviment
            .innerJoinAndSelect(
                'customer.customerPlan',
                'customerPlan'
            )
            .innerJoinAndSelect(
                'customerPlan.plan',
                'plan'
            )
            .take(parseInt(process.env.APP_PAGINATION_QTD_ITEMS, 0))
            .skip(
                parseInt(process.env.APP_PAGINATION_QTD_ITEMS, 0) *
                (filter.page - 1),
            )
            .where('customerPlan.id_subsidiary = :idsub', {
                idsub: header.subsidiaryId,
            })
            .andWhere(filterPeriod)

        const [customer, total] = await queryBuilderMoviment.getManyAndCount();

        return { customer, total };
    }

    public async findListCustomers(header: UserDecoratorInterface, transactionalEntityManager: any = null, filter: any): Promise<any[]> {

        let queryBuilderMoviment: SelectQueryBuilder<Customer> = null;
        let filterPeriod = '';

        if (transactionalEntityManager) {
            queryBuilderMoviment = transactionalEntityManager.createQueryBuilder(
                CustomerPlan,
                'customer',
            );
        } else {
            queryBuilderMoviment = this.customerRepository.createQueryBuilder(
                'customer',
            );

            queryBuilderMoviment
                .select('customer.name', 'name')
                .addSelect('customer.cpf', 'cpf')
                .addSelect('plan.value', 'value')
                .addSelect('plan.parcel', 'renewal')
                .addSelect('customerPlan.date_renewal', 'date_renewal')
                .addSelect('customerPlan.date_review', 'date_review')
                .addSelect('ceiling(plan.parcel / 2)', 'review')
        }

        if (filter.dateStart && filter.dateEnd) {
            filterPeriod = `customerPlan.dateSignature BETWEEN '${filter.dateStart}' AND '${filter.dateEnd}'`;
        } else {
            filterPeriod = `1 = 1`;
        }

        queryBuilderMoviment
            .innerJoinAndSelect(
                'customer.customerPlan',
                'customerPlan'
            )
            .innerJoinAndSelect(
                'customerPlan.plan',
                'plan'
            )
            .where('customerPlan.id_subsidiary = :idsub', {
                idsub: header.subsidiaryId,
            })
            .andWhere(filterPeriod)

        const customer = await queryBuilderMoviment.getRawMany();


        for (const cust of customer) {

            let date_ = cust.date_renewal.toISOString().slice(0, 10);

            cust.date_renewal = date_.substring(8, 10) + '/' + date_.substring(5, 7) + '/' + date_.substring(0, 4);

            date_ = cust.date_review.toISOString().slice(0, 10);

            cust.date_review = date_.substring(8, 10) + '/' + date_.substring(5, 7) + '/' + date_.substring(0, 4);

        }

        return customer;
    }

    public async getReportCustomers(header: UserDecoratorInterface, filter: any): Promise<any> {

        let path1 = `${process.env.APP_HOST}:${process.env.APP_PORT}`;
        let path2 = `${process.env.EXPOSED_PATH_PDF}/cliente.pdf`;

        // const PDFDocument = require('pdfkit');
        const fs = require('fs');

        var PdfTable = require('voilab-pdf-table'),
            PdfDocument = require('pdfkit');

        // create a PDF from PDFKit, and a table from PDFTable
        var pdf = new PdfDocument()

        // Embed a font, set the font size, and render some text
        pdf
            // .font('src/fonts/UbuntuMono-Bold.ttf')
            .fontSize(18)
            .text('Relatório de Clientes Pop', {
                width: 410,
                align: 'center'
            });

        pdf.moveDown();

        pdf
            // .font('src/fonts/UbuntuMono-Bold.ttf')
            .fontSize(14)
            .text(`Periodo: ${filter.dateStart.substring(8, 10) + '/' + filter.dateStart.substring(5, 7) + '/' + filter.dateStart.substring(0, 4)} até ${filter.dateEnd.substring(8, 10) + '/' + filter.dateEnd.substring(5, 7) + '/' + filter.dateEnd.substring(0, 4)}`, {
                width: 410,
                align: 'center'
            });

        pdf.moveDown();

        let listCoustomers = await this.findListCustomers(header, null, filter);

        pdf
            // .font('src/fonts/UbuntuMono-Regular.ttf')
            .fontSize(14)
            .text(`${listCoustomers.length.toString()} Pops Ativos`, {
                width: 410,
                align: 'center'
            });

        pdf.moveDown();

        var table = new PdfTable(pdf, {
            bottomMargin: 30
        });

        table
            // add some plugins (here, a 'fit-to-width' for a column)
            .addPlugin(new (require('voilab-pdf-table/plugins/fitcolumn'))({
                column: 'name'
            }))
            // set defaults to your columns
            .setColumnsDefaults({
                headerBorder: 'B',
                align: 'right'
            })
            // add table columns
            .addColumns([
                {
                    id: 'name',
                    header: 'Cliente',
                    align: 'left'
                },
                {
                    id: 'value',
                    header: 'Valor repasse',
                    align: 'left',
                    width: 100
                },
                {
                    id: 'cpf',
                    header: 'Cpf',
                    align: 'left',
                    width: 100
                }
            ])
            // add events (here, we draw headers on each new page)
            .onPageAdded(function (tb) {
                tb.addHeader();
            });

        // if no page already exists in your PDF, do not forget to add one
        // pdf.addPage();

        // draw content, by passing data to the addBody method
        table.addBody(listCoustomers);

        pdf.pipe(fs.createWriteStream(`uploads/${path2}`));

        pdf.end();

        return `${path1}/${path2}`;
    }

    public async getReportCustomersMonthly(header: UserDecoratorInterface, filter: any): Promise<any> {

        let path1 = `${process.env.APP_HOST}:${process.env.APP_PORT}`;
        let path2 = `${process.env.EXPOSED_PATH_PDF}/cliente.pdf`;

        // const PDFDocument = require('pdfkit');
        const fs = require('fs');

        var PdfTable = require('voilab-pdf-table'),
            PdfDocument = require('pdfkit');

        // create a PDF from PDFKit, and a table from PDFTable
        var pdf = new PdfDocument()

        // Embed a font, set the font size, and render some text
        pdf
            // .font('src/fonts/UbuntuMono-Bold.ttf')
            .fontSize(18)
            .text('Relatório de Clientes Pop', {
                width: 410,
                align: 'center'
            });

        pdf.moveDown();

        pdf
            // .font('src/fonts/UbuntuMono-Bold.ttf')
            .fontSize(14)
            .text(`Periodo: ${filter.dateStart.substring(8, 10) + '/' + filter.dateStart.substring(5, 7) + '/' + filter.dateStart.substring(0, 4)} até ${filter.dateEnd.substring(8, 10) + '/' + filter.dateEnd.substring(5, 7) + '/' + filter.dateEnd.substring(0, 4)}`, {
                width: 410,
                align: 'center'
            });

        pdf.moveDown();

        let listCoustomers = await this.findListCustomers(header, null, filter);

        pdf
            // .font('src/fonts/UbuntuMono-Regular.ttf')
            .fontSize(14)
            .text(`${listCoustomers.length.toString()} Pops Ativos`, {
                width: 410,
                align: 'center'
            });

        pdf.moveDown();

        var table = new PdfTable(pdf, {
            bottomMargin: 30
        });

        table
            // add some plugins (here, a 'fit-to-width' for a column)
            .addPlugin(new (require('voilab-pdf-table/plugins/fitcolumn'))({
                column: 'name'
            }))
            // set defaults to your columns
            .setColumnsDefaults({
                headerBorder: 'B',
                align: 'right'
            })
            // add table columns
            .addColumns([
                {
                    id: 'name',
                    header: 'Cliente',
                    align: 'left'
                },
                {
                    id: 'date_review',
                    header: 'Revisão',
                    align: 'left',
                    width: 100
                },
                {
                    id: 'date_renewal',
                    header: 'Renovação',
                    align: 'left',
                    width: 100
                },
                {
                    id: 'review',
                    header: 'Revisão (Meses)',
                    align: 'left',
                    width: 100
                },
                {
                    id: 'renewal',
                    header: 'Renovação (Meses)',
                    align: 'left',
                    width: 100
                }
            ])
            // add events (here, we draw headers on each new page)
            .onPageAdded(function (tb) {
                tb.addHeader();
            });

        // if no page already exists in your PDF, do not forget to add one
        // pdf.addPage();

        // draw content, by passing data to the addBody method
        table.addBody(listCoustomers);

        pdf.pipe(fs.createWriteStream(`uploads/${path2}`));

        pdf.end();

        return `${path1}/${path2}`;
    }
}
