import { Plan } from './plan.entity';
import {
    Injectable,
    Inject,
    BadRequestException,
    Head,
    Header,
} from '@nestjs/common';
import { Repository, getManager } from 'typeorm';
import { UserDecoratorInterface } from '../user/decorator/user.decorator';

@Injectable()
export class PlanService {
    constructor(
        @Inject('Plan')
        private readonly planRepository: Repository<Plan>,
    ) {}

    public async findById(
        header: UserDecoratorInterface,
        idProduct: string,
    ): Promise<Plan> {
        let rate = await this.planRepository.findOne(idProduct);

        return rate;
    }

    public async findAll(
        header: UserDecoratorInterface,
        name?: string,
    ): Promise<Plan[]> {
        const categorys = await this.planRepository
            .createQueryBuilder('plan')
            // .where('plan.subsidiary = :idSubsidiary', {
            //     idSubsidiary: header.subsidiaryId,
            // })
            .andWhere('plan.active = :status', { status: true })
            .getMany();

        return categorys;
    }

    public async findByName(
        header: UserDecoratorInterface,
        name?: string,
    ): Promise<Plan[]> {
        const categorys = await this.planRepository
            .createQueryBuilder('plan')
            // .where('plan.subsidiary = :idSubsidiary', {
            //     idSubsidiary: header.subsidiaryId,
            // })
            .andWhere(`plan.name = :query`, { query: name })
            .getMany();

        return categorys;
    }

    public async insert(
        header: UserDecoratorInterface,
        plan: Plan,
    ): Promise<Plan> {
        let name = plan.name;

        const rate = await this.findByName(header, name);

        if (rate.length > 0) {
            throw new BadRequestException('Plan is already');
        }

        // if (!plan.subsidiary) {
        //     plan.subsidiary = header.subsidiaryId;
        // }

        const insert = await this.planRepository.save(plan);

        return insert;
    }

    public async update(
        header: UserDecoratorInterface,
        plan: Plan,
    ): Promise<Plan> {
        const rate = await this.findById(header, plan.id);

        if (!rate) {
            throw new BadRequestException('Plan not found');
        }

        await this.planRepository.update(plan.id, plan);

        return this.findById(header, plan.id.toString());
    }

    public async enable(
        header: UserDecoratorInterface,
        plan: Plan,
    ): Promise<Plan> {
        const rate = await this.findById(header, plan.id);

        if (!rate) {
            throw new BadRequestException('Plan not found');
        }
        if (rate.active) {
            throw new BadRequestException('Plan is already active');
        }

        plan.active = true;

        return await this.update(header, plan);
    }

    public async disable(
        header: UserDecoratorInterface,
        plan: Plan,
    ): Promise<Plan> {
        const rate = await this.findById(header, plan.id);

        if (!rate) {
            throw new BadRequestException('Plan not found');
        }

        if (!rate.active) {
            throw new BadRequestException('Plan is already disabled');
        }

        rate.active = false;

        return await this.update(header, rate);
    }

    public async delete(
        header: UserDecoratorInterface,
        id: string,
    ): Promise<any> {
        const del = await this.planRepository.delete(id);

        return del;
    }
}
