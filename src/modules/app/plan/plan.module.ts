import { MySQLModule } from 'src/database/mysql/mysql.module';
import { Module } from '@nestjs/common';

import { PlanService } from './plan.service';
import { planRepository } from './plan.repository';
import { PlanController } from './plan.controller';

@Module({
    imports: [MySQLModule],
    controllers: [PlanController],
    providers: [
        ...planRepository,
        { provide: PlanService, useClass: PlanService },
    ],
    exports: [PlanService],
})
export class PlanModule {}
