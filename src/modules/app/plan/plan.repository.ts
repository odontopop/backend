import { Connection } from 'typeorm';
import { Plan } from './plan.entity';

export const planRepository = [
    {
        provide: 'Plan',
        useFactory: (connection: Connection) => connection.getRepository(Plan),
        inject: ['DATABASE_CONNECTION'],
    },
];
