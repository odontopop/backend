import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    ManyToOne,
    JoinColumn,
    JoinTable,
    OneToMany,
    AfterLoad,
} from 'typeorm';

import { IsString, IsNotEmpty, IsBoolean, IsNumber } from 'class-validator';
import { SubsidiaryPlan } from '../subsidiary-plan/subsidiary-plan.entity';

@Entity({ name: 'plan' })
export class Plan {
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column({ name: 'name', length: 200, nullable: false, default: '' })
    @IsString({ message: 'Name is String' })
    @IsNotEmpty({ message: 'Name cannot be empty' })
    public name: string;

    @IsNumber()
    @IsNotEmpty({ message: 'Value cannot be empty' })
    @Column({ type: 'double', nullable: false })
    public value: number;

    @IsNumber()
    @IsNotEmpty({ message: 'Parcela cannot be empty' })
    @Column({ type: 'double', nullable: false })
    public parcel: number;

    @IsNumber()
    @IsNotEmpty({ message: 'Percent cannot be empty' })
    @Column({ type: 'double', nullable: false })
    public percent: number;

    @Column({ type: 'boolean', nullable: false, default: true })
    @IsBoolean({ message: 'Active is boolean' })
    @IsNotEmpty({ message: 'Active cannot be empty' })
    public active: boolean;

    @OneToMany((type) => SubsidiaryPlan, (subsidiaryPlan) => subsidiaryPlan.plan, {
        nullable: false,
        onDelete: 'CASCADE',
    })
    public subsidiaryPlan: SubsidiaryPlan | string;
}