import {
    Controller,
    Get,
    HttpStatus,
    Headers,
    Res,
    Body,
    Post,
    Param,
    Put,
    Delete,
} from '@nestjs/common';
import { PlanService } from './plan.service';
import { Plan } from './plan.entity';
import { User } from '../user/decorator/user.decorator';

@Controller('api/v1/plan')
export class PlanController {
    constructor(private readonly planService: PlanService) { }

    @Post()
    public async post(
        @Res() res,
        @User({ validSubsidiary: false }) header,
        @Body() plan: Plan,
    ) {
        const save = await this.planService.insert(header, plan);
        return res.status(HttpStatus.OK).json({
            message: `Plan Sucessfully Created`,
            status: true,
            data: save,
        });
    }

    @Put()
    public async put(@Res() res, @User({ validSubsidiary: false, validToken: false }) header, @Body() plan: Plan) {
        const result = await this.planService.update(header, plan);
        return res.status(HttpStatus.OK).json({
            message: `Plan Sucessfully Updated`,
            status: true,
            data: result,
        });
    }

    @Put('/enable')
    public async enable(@Res() res, @User() header, @Body() plan: Plan) {
        const result = await this.planService.enable(header, plan);
        return res.status(HttpStatus.OK).json({
            message: `Plan Sucessfully Enabled`,
            status: true,
            data: result,
        });
    }

    @Put('/disable')
    public async disable(@Res() res, @User() header, @Body() plan: Plan) {
        const result = await this.planService.disable(header, plan);
        return res.status(HttpStatus.OK).json({
            message: `Plan Sucessfully Disabled`,
            status: true,
            data: result,
        });
    }

    @Delete('/:id')
    async delete(@Res() response, @User() header, @Param('id') id: string) {
        const remove = await this.planService.delete(header, id);
        return response.status(HttpStatus.OK).json({
            message: `Plan has been Deleted`,
            status: true,
            data: remove,
        });
    }

    @Get()
    public async listActive(
        @Res() res,
        @User({ validSubsidiary: false, validToken: false }) header,
        @Body() plan: Plan,
    ) {
        const result = await this.planService.findAll(header);
        return res.status(HttpStatus.OK).json({
            message: `List of Products Actives`,
            status: true,
            data: result,
        });
    }
}
