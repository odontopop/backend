import {
    Controller,
    Post,
    Headers,
    Res,
    Body,
    HttpStatus,
    Put,
    Param,
    Get,
    UsePipes,
    ValidationPipe,
} from '@nestjs/common';
import { SubsidiaryService } from './subsidiary.service';
import { Subsidiary } from './subsidiary.entity';
import { User } from '../user/decorator/user.decorator';
import { SubsidiaryPlanDto } from './subsidiary.dto';

@Controller('api/v1/subsidiary')
export class SubsidiaryController {
    constructor(private readonly subsidiaryRepository: SubsidiaryService) { }

    @Post()
    @UsePipes(ValidationPipe)
    public async post(
        @Res() res,
        @User({ validSubsidiary: false }) header,
        @Body() subsidiary: SubsidiaryPlanDto,
    ) {
        const save = await this.subsidiaryRepository.save(subsidiary);
        return res.status(HttpStatus.OK).json({
            message: `Subsidiary Sucessfully Created`,
            status: true,
            data: save,
        });
    }

    @Put()
    @UsePipes(ValidationPipe)
    public async put(
        @Res() res,
        @User({ validSubsidiary: false }) header,
        @Body() subsidiary: SubsidiaryPlanDto,
    ) {
        const result = await this.subsidiaryRepository.save(subsidiary);
        return res.status(HttpStatus.OK).json({
            message: `Subsidiary Sucessfully Updated`,
            status: true,
            data: result,
        });
    }

    @Put('/enable/:id')
    @UsePipes(ValidationPipe)
    public async enable(@Res() res, @Param('id') param: string) {
        const result = await this.subsidiaryRepository.enable(param);
        return res.status(HttpStatus.OK).json({
            message: `Subsidiary Sucessfully Enabled`,
            status: true,
            data: result,
        });
    }

    @Put('/disable/:id')
    @UsePipes(ValidationPipe)
    public async disable(@Res() res, @Param('id') param: string) {
        const result = await this.subsidiaryRepository.disable(param);
        return res.status(HttpStatus.OK).json({
            message: `Subsidiary Sucessfully Disabled`,
            status: true,
            data: result,
        });
    }

    @Get('/search/:query')
    public async getBySearch(@Res() res, @Param('query') param: string) {
        const result = await this.subsidiaryRepository.findByLike(param);
        return res.status(HttpStatus.OK).json({
            message: `List of Subsidiarys in Query`,
            status: true,
            data: result,
        });
    }

    @Get('/active')
    public async listActive(@Res() res) {
        const result = await this.subsidiaryRepository.findAll(true);
        return res.status(HttpStatus.OK).json({
            message: `List of Subsidiarys Actives`,
            status: true,
            data: result,
        });
    }

    @Get('/:id')
    public async getById(@Res() res, @Param('id') param: string) {
        const result = await this.subsidiaryRepository.findById(param);
        return res.status(HttpStatus.OK).json({
            message: `Subsidiary by Id`,
            status: true,
            data: result,
        });
    }

    @Get()
    public async listAll(@Res() res) {
        const result = await this.subsidiaryRepository.findAll();
        return res.status(HttpStatus.OK).json({
            message: `List of Subsidiarys`,
            status: true,
            data: result,
        });
    }
}
