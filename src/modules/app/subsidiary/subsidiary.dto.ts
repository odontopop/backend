import { Column } from 'typeorm';
import { IsString, IsNotEmpty, IsEnum, IsBoolean } from 'class-validator';
import { PlanDto } from '../plan/dto/plan.dto';

export class SubsidiaryPlanDto {

    public id: string;

    @IsString({ message: 'Name is String' })
    @IsNotEmpty({ message: 'Name cannot be empty' })
    public name: string;

    @IsString({ message: 'CNPJ is String' })
    @IsNotEmpty({ message: 'CNPJ cannot be empty' })
    public cnpj: string;

    @IsBoolean({ message: 'Active is boolean' })
    public active: boolean;

    public plan: PlanDto[];
}
