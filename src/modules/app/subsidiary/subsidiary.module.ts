import { MySQLModule } from 'src/database/mysql/mysql.module';
import { Module } from '@nestjs/common';

import { subsidiaryRepository } from './subsidiary.repository';
import { SubsidiaryService } from './subsidiary.service';
import { SubsidiaryController } from './subsidiary.controller';

@Module({
  imports: [MySQLModule],
  controllers: [SubsidiaryController],
  providers: [
    ...subsidiaryRepository,
    { provide: SubsidiaryService, useClass: SubsidiaryService },
  ],
  exports: [SubsidiaryService],
})
export class SubsidiaryModule {}
