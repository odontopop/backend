import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    OneToMany,
    JoinColumn,
    ManyToOne,
    OneToOne,
    BeforeInsert,
} from 'typeorm';
import {
    IsString,
    IsNotEmpty,
    IsBoolean,
    IsPhoneNumber,
    IsEmail,
} from 'class-validator';

// Models
import { Company } from '../company/dto/company.entity';
import { User } from '../user/user.entity';
import { SubsidiaryPlan } from '../subsidiary-plan/subsidiary-plan.entity';
import { CustomerPlan } from '../customer-plan/customer-plan.entity';

@Entity({ name: 'subsidiary' })
export class Subsidiary {
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @IsString({ message: 'Name is String' })
    @IsNotEmpty({ message: 'Name cannot be empty' })
    @Column({ name: 'name', length: 200, nullable: false })
    public name: string;

    @IsString({ message: 'Nickname is String' })
    @IsNotEmpty({ message: 'Nickname cannot be empty' })
    @Column({ name: 'nickname', length: 200, nullable: true })
    public nickname: string;

    @IsString({ message: 'CNPJ is String' })
    @IsNotEmpty({ message: 'CNPJ cannot be empty' })
    @Column({ name: 'cnpj', length: 200, nullable: false })
    public cnpj: string;

    // @IsPhoneNumber('+55', { message: 'Phone is Invalid add +55' })
    @IsNotEmpty({ message: 'Phone cannot be empty' })
    @Column({ name: 'phone', length: 200, nullable: true })
    public phone: string;

    @IsEmail({}, { message: 'E-mail is inválid' })
    @IsNotEmpty({ message: 'E-mail cannot be empty' })
    @Column({ name: 'mail', length: 200, nullable: true })
    public mail: string;

    @IsBoolean({ message: 'Active is boolean' })
    @Column({ type: 'boolean', nullable: false, default: true })
    public active: boolean;

    @OneToMany((type) => User, (user) => user.subsidiary, {
        nullable: true,
        cascade: true,
    })
    public user: User;

    @OneToMany((type) => SubsidiaryPlan, (subsidiaryPlan) => subsidiaryPlan.subsidiary, {
        nullable: true,
        cascade: true,
    })
    public subsidiaryPlan: SubsidiaryPlan[];

    @OneToMany((type) => CustomerPlan, (customerPlan) => customerPlan.subsidiary, {
        nullable: true,
        cascade: true,
    })
    public customerPlan: CustomerPlan;

    @BeforeInsert() async prepareDataInsert() {
        if (this.phone) {
            this.phone = this.phone.replace(/[^0-9]/g, '');
        }
    }
}
