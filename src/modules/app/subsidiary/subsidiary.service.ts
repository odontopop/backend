import { Injectable, Inject, BadRequestException } from '@nestjs/common';
import { getManager, Repository } from 'typeorm';

import { SubsidiaryPlan } from '../subsidiary-plan/subsidiary-plan.entity';
import { SubsidiaryPlanDto } from './subsidiary.dto';
import { Subsidiary } from './subsidiary.entity';

@Injectable()
export class SubsidiaryService {
    constructor(
        @Inject('SUBSIDIARY_REPOSITORY')
        private readonly subsidiaryRepository: Repository<Subsidiary>,
    ) { }

    public async findAll(enabled?: boolean): Promise<Subsidiary[]> {
        return await this.findById();
    }

    public async findByLike(paramQuery?: string): Promise<Subsidiary[]> {
        if (paramQuery.length > 0) {
            const holdings = await this.subsidiaryRepository
                .createQueryBuilder('subsidiaries')
                .andWhere(`subsidiaries.name like :query`, {
                    query: '%' + paramQuery + '%',
                })
                .orWhere(`subsidiaries.id like :query`, {
                    query: '%' + paramQuery + '%',
                })
                .getMany();
            return holdings;
        } else {
            return await this.findAll();
        }
    }

    public async findById(id: string = null): Promise<any> {
        let queryBuilderMoviment: any = this.subsidiaryRepository.createQueryBuilder(
            'subsidiary',
        );

        queryBuilderMoviment.innerJoinAndSelect(
            'subsidiary.subsidiaryPlan',
            'subsidiaryPlan',
        );

        queryBuilderMoviment.innerJoinAndSelect(
            'subsidiaryPlan.plan',
            'plan',
        );

        if (id) {
            queryBuilderMoviment.where('subsidiary.id = :id', { id });
        }

        let subsidiary = await queryBuilderMoviment.getMany();

        for (const sub of subsidiary) {
            for (const item of sub.subsidiaryPlan) {
                if (!sub.plan) {
                    sub.plan = [];
                }
                if (item.active) {
                    sub.plan.push(item.plan);
                }
            }

            delete sub.subsidiaryPlan;
        }

        if (id) {
            if (subsidiary) {
                subsidiary = subsidiary[0];
            }
        }

        return subsidiary;
    }

    public async findByCnpj(cnpj: string): Promise<Subsidiary> {
        return await this.subsidiaryRepository.findOne({ where: { cnpj: cnpj } });
    }

    public async findByIdOrder(idOrder: string): Promise<Subsidiary> {
        const orders = await this.subsidiaryRepository
            .createQueryBuilder('subsidiary')
            .innerJoinAndSelect('subsidiary.order', 'order')
            .where('order.id   = :iDOrder', {
                iDOrder: idOrder,
            })
            .getOne();

        return orders;
    }

    public async save(subsidiaryDto: SubsidiaryPlanDto): Promise<any> {
        let subsidiaryLoad: Partial<Subsidiary> = {};

        await getManager().transaction(async (transactionalEntityManager) => {

            let subsidiary = await this.findByCnpj(subsidiaryDto.cnpj);

            if (subsidiary) {

                subsidiaryLoad = {
                    name: subsidiaryDto.name,
                    cnpj: subsidiaryDto.cnpj,
                    active: subsidiaryDto.active,
                    id: subsidiary.id
                }
            } else {
                subsidiaryLoad = {
                    name: subsidiaryDto.name,
                    cnpj: subsidiaryDto.cnpj,
                    active: subsidiaryDto.active,
                    id: subsidiaryDto.id ? subsidiaryDto.id : undefined
                }
            }

            if (subsidiaryLoad.id) {
                await transactionalEntityManager
                    .createQueryBuilder(SubsidiaryPlan, 'subsidiaryPlan')
                    .update()
                    .set({
                        active: false
                    })
                    .where('id_subsidiary = :id', { id: subsidiaryLoad.id })
                    .execute();
            }

            subsidiaryLoad = await transactionalEntityManager.create(
                Subsidiary,
                subsidiaryLoad,
            );

            subsidiaryLoad = await transactionalEntityManager.save(
                subsidiaryLoad,
            );

            if (subsidiaryDto.plan.length > 0) {
                for (const item of subsidiaryDto.plan) {
                    let subsidiaryDB = await this.GetPlanOfSubsidyary(
                        transactionalEntityManager,
                        subsidiaryLoad.cnpj,
                        item.id,
                    );

                    if (subsidiaryDB.length > 0) {

                        for (const itemDB of subsidiaryDB) {
                            if (subsidiaryDB.length > 0) {
                                let subsidiaryPlanLoad: Partial<SubsidiaryPlan> = {
                                    id: itemDB.id,
                                    active: true,
                                };

                                subsidiaryPlanLoad = await transactionalEntityManager.create(
                                    SubsidiaryPlan,
                                    subsidiaryPlanLoad,
                                );

                                subsidiaryPlanLoad = await transactionalEntityManager.save(
                                    subsidiaryPlanLoad,
                                );
                            }
                        }

                    } else {
                        let subsidiaryPlanLoad: Partial<SubsidiaryPlan> = {
                            subsidiary: subsidiaryLoad.id,
                            plan: item.id,
                            active: true,
                        };

                        subsidiaryPlanLoad = await transactionalEntityManager.create(
                            SubsidiaryPlan,
                            subsidiaryPlanLoad,
                        );

                        subsidiaryPlanLoad = await transactionalEntityManager.save(
                            subsidiaryPlanLoad,
                        );
                    }
                }
            } else {
                let subsidiaryDB = await this.GetPlanOfSubsidyary(
                    transactionalEntityManager,
                    subsidiaryLoad.cnpj,
                    null,
                );

                for (const itemDB of subsidiaryDB) {
                    if (subsidiaryDB.length > 0) {
                        let subsidiaryPlanLoad: Partial<SubsidiaryPlan> = {
                            id: itemDB.id,
                            plan: itemDB.plan,
                            active: false,
                        };

                        subsidiaryPlanLoad = await transactionalEntityManager.create(
                            SubsidiaryPlan,
                            subsidiaryPlanLoad,
                        );

                        subsidiaryPlanLoad = await transactionalEntityManager.save(
                            subsidiaryPlanLoad,
                        );
                    }
                }
            }
        });

        return await this.findById(subsidiaryLoad.id);
    }

    public async GetPlanOfSubsidyary(
        transactionalEntityManager: any = null,
        cnpj: string,
        id: string = null,
    ): Promise<SubsidiaryPlan[]> {
        let queryBuilderMoviment = transactionalEntityManager.createQueryBuilder(
            SubsidiaryPlan,
            'subsidiaryPLan',
        );

        queryBuilderMoviment.innerJoin(
            'subsidiaryPLan.subsidiary',
            'subsidiary',
        );

        queryBuilderMoviment.innerJoin(
            'subsidiaryPLan.plan',
            'plan',
        );

        queryBuilderMoviment.where('subsidiary.cnpj = :cnpj', { cnpj });

        if (id) {
            queryBuilderMoviment.andWhere('plan.id = :id', { id });
        }

        let subsidiaryPLanBD = await queryBuilderMoviment.getMany();

        return subsidiaryPLanBD;
    }

    public async update(subsidiary: Subsidiary): Promise<Subsidiary> {
        await this.subsidiaryRepository.update(subsidiary.id, subsidiary);
        return this.findById(subsidiary.id.toString());
    }

    public async enable(id: string): Promise<Subsidiary> {
        const subsidiary: Subsidiary = await this.findById(id);
        if (!subsidiary) {
            throw new BadRequestException('Subsidiary not found');
        }
        if (subsidiary.active) {
            throw new BadRequestException('Subsidiary is already active');
        }

        subsidiary.active = true;
        return await this.update(subsidiary);
    }

    public async disable(id: string): Promise<Subsidiary> {
        const subsidiary: Subsidiary = await this.findById(id);
        if (!subsidiary) {
            throw new BadRequestException('Subsidiary not found');
        }

        if (!subsidiary.active) {
            throw new BadRequestException('Subsidiary is already disabled');
        }

        subsidiary.active = false;
        return await this.update(subsidiary);
    }
}
