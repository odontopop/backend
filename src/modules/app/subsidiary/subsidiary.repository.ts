import { Connection } from 'typeorm';
import { Subsidiary } from './subsidiary.entity';

export const subsidiaryRepository = [
  {
    provide: 'SUBSIDIARY_REPOSITORY',
    useFactory: (connection: Connection) =>
      connection.getRepository(Subsidiary),
    inject: ['DATABASE_CONNECTION'],
  },
];
